<div class="page-header section">
	<div class="fancy">
	    <?php
		  $town = get_field('townscape_site_town', 'option');
		  $state = get_field('townscape_site_state', 'option');
		  $country = get_field('townscape_site_country', 'option');
		  $location = $town . ', ' . $state . ', ' . $country;
		?>
        <h2>Welcome<br/><span>to</span></h2>
        <h1><?php echo $town;?></h1>
        <h3><?php echo $state;?>, <br/><span><?php echo $country;?></span></h3>
    </div>
    <?php if ( has_post_thumbnail() ) { ?>
		<figure>
			<?php the_post_thumbnail('townscape_header', array('class' => 'img-responsive'));?>
			<figcaption class="caption">"<?php echo get_post(get_post_thumbnail_id())->post_title;?>" - <?php echo get_post(get_post_thumbnail_id())->post_excerpt; ?></figcaption>
		</figure>
	<?php } else { ?>
		<figure>
            <img src="http://placehold.it/860x300&text=<?php echo $town;?>" class="attachment-full img-responsive" alt="header-<?php echo $town;?>">
        </figure>
	<?php } ?>
	<?php while (have_posts()) : the_post(); ?>
    <article class="content">
		<div class="page-content">
			<?php the_content();?>
		</div>
    </article>
	<?php endwhile; ?>
</div>

<div class="section-title">
	<h2>What can we help you find today?</h2>
</div>

<?php
$menu_name = 'button_navigation';

if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
	$menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
	$menu_items = wp_get_nav_menu_items($menu->term_id);

	$menu_list = '<div class="tiles section">';
	foreach ( (array) $menu_items as $key => $menu_item ) {
	    $title = $menu_item->title;
	    $url = $menu_item->url;
	    $description = $menu_item->description;
	    $menu_icon = $menu_item->classes[0];

	    $menu_list .= '<div class="tile">';
			$menu_list .= '<a href="'.$url.'">';
			$menu_list .= '<span class="'. $menu_icon . '"></span>';
				$menu_list .= '<h3>' . $title . '</h3>';
				$menu_list .= '<p>'.$description.'</p>';
				$menu_list .= '<button href="'.$url.'" class="btn btn-tile">View More</button>';
			$menu_list .= '</a>';
		$menu_list .= '</div>';
	}
	$menu_list .= '</div>';

	echo $menu_list;
}
?>

<?php
$args = array(
	'post_type' => 'business',
	'meta_query' => array(
        array(
            'key' => 'townscape_related_adverts',
            'value' => 0,
            'type' => 'NUMERIC',
            'compare' => '>'
        )
    )
);
$query1 = new WP_Query( $args );
if($query1->have_posts()) {?>
	<div class="featured-listings section">
	<h3>Featured Listings</h3>
	    <?php while ( $query1->have_posts() ):$query1->the_post();?>
	    	<?php
			$card_class = get_post_type();
			$card_classes = array(
				'card',
				'mini',
				$card_class
				);
			?>
        <a alt="View More about <?php the_title();?>" href="<?php the_permalink();?>" >
	        <article <?php post_class($card_classes);?>>
	            <div class="page-content <?php echo $bookmarked_class?>">
			        <?php if ( has_post_thumbnail() ) {
			            the_post_thumbnail('townscape_thumb', array('class' => 'listing-main-image'));
			        }
			        else{
			            $title = get_the_title();
			            $stringtitle = str_replace(" ", "+", $title);
			            echo '<img src="http://placehold.it/300x195&text='.$stringtitle.'" class="listing-main-image">';
			        } ?>
		            <h4 class="listing-name title">
		                <?php the_title();?>
		            </h4>
				</div>
	        </article>
        </a>
	    <?php endwhile;?>
	</div><!--/listings-section-->
<?php } wp_reset_postdata();?>

<div class="tiles map section">
	<script src='https://api.tiles.mapbox.com/mapbox.js/v2.1.9/mapbox.js'></script>
	<link href='https://api.tiles.mapbox.com/mapbox.js/v2.1.9/mapbox.css' rel='stylesheet' />
	<style>
	  #main-map { position:relative; top:0; bottom:0; width:90%; height:400px; }
		.map.section h3 {
		  font-size: 1.5em;
		  margin: 20px 0;
		  background: #E9F8E9;
		  padding: 20px;
		}
		.map.section .directions {
			padding: 20px;
			font-size: 1.4em;
		  text-decoration: underline;
		}
	</style>

	<h3>Where is Kenmare?</h3>
	<div id="main-map" class="kenmare-map"></div>
	<div class="directions"><a href="https://www.google.com/maps/dir/Current+Location/51.880692, -9.584340" alt="Get Directions">Get Directions</a></div>

	<script>
		L.mapbox.accessToken = 'pk.eyJ1Ijoia2VubWFyZWNyZWF0aXZlIiwiYSI6IkhJUlB4MTAifQ.72y0YoPhg0ZS33tVStGxZQ';
		var map = L.mapbox.map('main-map', 'kenmarecreative.hjp8kh05')
		    .setView([51.880692, -9.584340], 8);
				L.marker([51.880692, -9.584340], {
				    icon: L.mapbox.marker.icon({
				        'marker-size': 'large',
				        'marker-symbol': 'heart',
				        'marker-color': '#d00'
				    })
				}).addTo(map);
	</script>
</div>

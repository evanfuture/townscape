<?php
/*
Template Name: Business Export Template
*/
?>

<?php get_template_part('templates/page', 'header'); ?>

<?php if (have_posts()) {?>
<div class="listings section">
<style type="text/css">
table  {border-collapse:collapse;border-spacing:0;border-color:#aabcfe;margin:0px auto;}
table td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aabcfe;color:#669;background-color:#e8edff;}
table th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aabcfe;color:#039;background-color:#b9c9fe;}
table td{background-color:#D2E4FC;}
</style>

    <h1>Everything</h1>

    <?php
    $taxonomies = array( 'business_type' );
        $args = array(
            'orderby'           => 'ID',
            'order'             => 'ASC',
            'hide_empty'        => false,
            'parent'            => 0
        );
    $terms = get_terms($taxonomies, $args);
    $gem_subtype_slugs = array();
    foreach ( $terms as $term ) {
        ?>
            <h2><?php echo $term->name;?></h2>
            <?php
                $gem_type_children = get_term_children( $term->term_id, 'business_type' );
                if(!empty($gem_type_children)){?>
                    <?php foreach ( $gem_type_children as $gem_type ) {
                        $term2 = get_term_by( 'id', $gem_type, 'business_type' );
                        ?>
                        <h3><?php echo $term2->name;?></h3>
                        <table>
                          <tr>
                            <th>Name</th>
                            <th>address1</th>
                            <th>address2</th>
                            <th>phone</th>
                            <th>email</th>
                            <th>url</th>
                          </tr>

                        <?php
                        $args = array(
                            'post_type' => 'business',
                            'orderby' => 'title',
                            'order'   => 'ASC',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'business_type',
                                    'field'    => 'slug',
                                    'terms'    => $term2->slug,
                                ),
                            )
                        );
                        $query2 = new WP_Query( $args );?>
                          <?php if($query2->have_posts()): while ( $query2->have_posts() ):$query2->the_post();
                          	$title = get_the_title();
                            $phone = get_field('townscape_phone');
                            $phone = preg_replace('/\s+/', '', $phone);
                            $email = get_field('townscape_email');
                            $url = get_field('townscape_url');
                            $address1 = get_field('townscape_address1');
                            $address2 = get_field('townscape_address2');?>
                          <tr>
                            <td><?php edit_post_link( $title);?></td>
                            <td><input value="<?php echo $address1;?>"/></td>
                            <td><input value="<?php echo $address2;?>"/></td>
                            <td><input value="<?php echo $phone;?>"/></td>
                            <td><input value="<?php echo $email;?>"/></td>
                            <td><input value="<?php echo $url;?>"/></td>
                          </tr>
                          <?php endwhile; endif; wp_reset_postdata();?>
                        </table>
                    <?php }
            } ?>
        <?php
    }?>

</div><!--/listings-section-->

<?php } else{ get_template_part('templates/no', 'results'); } ?>
<?php if ( is_page_template( 'template-html.php' ) || is_singular( 'sa_invoice' ) || is_singular('sa_estimate' )) {
  include roots_template_path();
} else { ?>

<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?>>

  <!--[if lt IE 8]>
    <div class="alert alert-warning">
      <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'roots'); ?>
    </div>
  <![endif]-->

  <?php
    get_template_part('templates/header');
    get_template_part('templates/breadcrumbs');

    include roots_template_path();

    get_template_part('templates/footer');
  ?>

</body>
</html>

<?php }?>
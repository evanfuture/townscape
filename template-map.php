<?php
/*
Template Name: Custom Map
*/
?>
<script src='https://api.tiles.mapbox.com/mapbox.js/v2.0.0/mapbox.js'></script>
<link href='https://api.tiles.mapbox.com/mapbox.js/v2.0.0/mapbox.css' rel='stylesheet' />
<style>
  #map { width:100%; height:600px;}
</style>
<style>
.card.mini.map {
  position:relative;
  top:-600px;
  left:573px;
  }
.card.mini.map small {
    font-size: 70%;
  }


.checkbox-pill input[type=checkbox] {
display: none;
}
.checkbox-pill input[type=checkbox]:checked + label {
background-color: #3bb2d0;
}
.checkbox-pill .button {
    text-align: center;
color: #fff;
display: inline-block;
height: 40px;
margin: 0px;
padding: 10px;
position: relative;
border: none;
cursor: pointer;
border-radius: 3px;
white-space: nowrap;
text-overflow: ellipsis;
font-family: 'Open Sans Bold', sans-serif;
line-height: 20px;
font-size: 12px;
}
.checkbox-pill input[type=checkbox] + label:before {
background-color: rgba(0,0,0,0.25);
border-radius: 2px;
}
.icon.check:before {
content: "\e60a";
margin-right: 5px;
}
.icon:before {
font-family: 'townscape-iconic';
display: inline-block;
width: 20px;
height: 20px;
font-size: 20px;
color: inherit;
vertical-align: top;
background-size: 4320px 60px;
speak: none;
font-style: normal;
font-weight: normal;
font-variant: normal;
text-transform: none;
line-height: 1;
}
</style>


<?php get_template_part('templates/page', 'header'); ?>
<div class="section sorting">

    <div id='filters' class='ui-select pad2 checkbox-pill'>
    <?php
      $terms = get_terms( 'business_type', array( 'parent' => 0 ));
      foreach ($terms as $term) {
        echo '<input type=\'checkbox\' checked=checked class=\'filter\' name=\'filter\' id=\'' . $term->slug . '\' value=\'' . $term->slug . '\' /><label for=\'' . $term->slug . '\' class=\'button icon check\'>' . $term->name . '</label>';
        ?><!--<input type="radio" name="business_type" id="<?php echo $term->slug;?>" checked="checked">
      <label for="<?php echo $term->slug;?>" class="button icon check"><?php echo $term->name;?></label>--><?php
        }

    ?>
    </div>
</div>

<div class="section feature">
    <div id='map'></div>
    <article id='info' class='card mini map'></article>
</div>


<script>

L.mapbox.accessToken = 'pk.eyJ1Ijoia2VubWFyZWNyZWF0aXZlIiwiYSI6IkhJUlB4MTAifQ.72y0YoPhg0ZS33tVStGxZQ';

var info = document.getElementById('info');
var filters = document.getElementById('filters');

var map = L.mapbox.map('map', 'kenmarecreative.hjp8kh05')
.setView([51.8779594, -9.5835767], 15);
var myLayer = L.mapbox.featureLayer().addTo(map);

myLayer.setGeoJSON({
    type: "FeatureCollection",
    features: [
<?php
    $args = array('post_type'=>'business');
    $the_query = new WP_Query( $args );
    if($the_query->have_posts()){while($the_query->have_posts()){$the_query->the_post();
        $latitude = get_field('townscape_gps_lat');
        $longitude = get_field('townscape_gps_lng');
        if (!empty($longitude)){
            $title = get_the_title();
            $subtitle = get_field('townscape_subtitle');
            $permalink = get_permalink();
            $terms = get_the_terms( $post->ID, 'business_type' );
                if ( !empty( $terms ) ){
                    // get the first term
                    $term = array_shift( $terms );
                    $parent = get_term($term->parent, 'business_type');
                    $business_type = $parent->slug;

                }
            $content1 = '<div class="page-content"><div class="right-half">';
            if ( has_post_thumbnail() ) {
                $content1 .= get_the_post_thumbnail($the_query->ID,'townscape_thumb', array('class' => 'listing-main-image'));
            }
            else{
                $stringtitle = str_replace(" ", "+", $title);
                $content1 .= '<img src="http://placehold.it/300x195&text='.$stringtitle.'" class="listing-main-image">';
            }
            $content1 .= '</div><div class="left-half"><div class="card-name">';
            $content1 .= '<h2 class="listing-name title">'.$title.'</h2>';
            if( !empty($subtitle)) {
                $content1 .= '<p>'.$subtitle.'</p>';
            }
            $site_lat = get_field('townscape_site_gps_lat', 'option');
            $site_lng = get_field('townscape_site_gps_lng', 'option');
            $distance = townscape_distance($latitude, $longitude, $site_lat, $site_lng, "M");
            $rounded_distance = ceil($distance);
            if($rounded_distance <=1 ){$content1 .= '<small>In Town</small>';} else {
            $content1 .= '<small>About '.$rounded_distance . " Miles from Town.</small>";
            }
            $content1 .= '</div></div></div>';
            $content1 .= '<a alt="View More about '.$title.'" href="'.$permalink.'" class="more-link primary-button">View More</a>';

            ?>
            {
                type: 'Feature',
                geometry: {
                    type: 'Point',
                    coordinates: [<?php echo $longitude;?>, <?php echo $latitude;?>]
                },
                properties: {
                    "title": "<?php echo $title;?>",
                    "description": '<?php echo $content1;?>',
                    'marker-color': '#34e2e3',
                    'marker-symbol': '<?php echo $business_type;?>'
                }
            },
        <?php }
    }}
    /* Restore original Post Data */
    wp_reset_postdata();

    ?>
    ]
});

var checkboxes = document.getElementsByClassName('filter');

function change() {
    // Find all checkboxes that are checked and build a list of their values
    var on = [];
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].checked) on.push(checkboxes[i].value);
    }
    // The filter function takes a GeoJSON feature object
    // and returns true to show it or false to hide it.
    myLayer.setFilter(function (f) {
        // check each marker's symbol to see if its value is in the list
        // of symbols that should be on, stored in the 'on' array
        return on.indexOf(f.properties['marker-symbol']) !== -1;
    });
    return false;
}

// When the form is touched, re-filter markers
filters.onchange = change;

// Initially filter the markers
change();


// Listen for individual marker clicks.
myLayer.on('click',function(e) {
    // Force the popup closed.
    e.layer.closePopup();

    var feature = e.layer.feature;
    var content = feature.properties.description;

    info.innerHTML = content;
        map.panTo(e.layer.getLatLng());

});

// Clear the tooltip when map is clicked.
map.on('click', empty);

// Trigger empty contents when the script
// has loaded on the page.
empty();

function empty() {
  info.innerHTML = '';
  }

</script>
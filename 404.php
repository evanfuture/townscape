<div class="page-header section">
    <div class="page-title">
        <h2>404 Error - Not Found</h2>
    </div>
   
    <div class="page-description">
	    <div class="alert alert-warning">
		 	<div class="alert-text">
			 	<p class="lead"><?php _e('Sorry, but the page you were trying to view does not exist.', 'roots'); ?></p>

				<p><?php _e('It looks like this was the result of either:', 'roots'); ?></p>
				<ul>
				  <li><?php _e('a mistyped address', 'roots'); ?></li>
				  <li><?php _e('an out-of-date link', 'roots'); ?></li>
				</ul>
			</div>

			<?php get_search_form(); ?>
		</div>
    </div>
</div>
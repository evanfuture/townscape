<?php

add_filter('acf/settings/save_json', 'townscape_acf_json_save_point');

function townscape_acf_json_save_point( $path ) {

    // update path
    $path = get_stylesheet_directory() . '/lib/acf-json';


    // return
    return $path;

}

function json_api_encode_acf($response)
{
    if (isset($response['posts'])) {
        foreach ($response['posts'] as $post) {
            json_api_add_acf($post); // Add specs to each post
        }
    }
    else if (isset($response['post'])) {
        json_api_add_acf($response['post']); // Add a specs property
    }

    return $response;
}

function json_api_add_acf(&$post)
{
    $post->acf = get_fields($post->id);
}



if( function_exists('register_field_group') ):
      $town = get_field('townscape_site_town', 'option');
      $state = get_field('townscape_site_state', 'option');
      $town_name = $town . ', ' . $state;
register_field_group(array (
  'key' => 'group_53d77faf04cce',
  'title' => 'Accommodation Fields',
  'fields' => array (
    array (
      'key' => 'field_53c3c7e27ba94',
      'label' => 'Add Booking.com Link?',
      'name' => 'townscape_booking_com',
      'prefix' => '',
      'type' => 'true_false',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'message' => '',
      'default_value' => 0,
    ),
    array (
      'key' => 'field_53c3b9b1b76d1',
      'label' => 'Booking.com url',
      'name' => 'townscape_booking_com_url',
      'prefix' => '',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => array (
        array (
          array (
            'field' => 'field_53c3c7e27ba94',
            'operator' => '==',
            'value' => '1',
          ),
        ),
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'formatting' => 'none',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
  ),
  'location' => array (
    array (
      array (
        'param' => 'post_taxonomy',
        'operator' => '==',
        'value' => 'business_type:hotels-kenmare',
      ),
    ),
    array (
      array (
        'param' => 'post_taxonomy',
        'operator' => '==',
        'value' => 'business_type:bandb-kenmare',
      ),
    ),
    array (
      array (
        'param' => 'post_taxonomy',
        'operator' => '==',
        'value' => 'business_type:self-catering-kenmare',
      ),
    ),
    array (
      array (
        'param' => 'post_taxonomy',
        'operator' => '==',
        'value' => 'business_type:camping-kenmare',
      ),
    ),
  ),
  'menu_order' => 0,
  'position' => 'acf_after_title',
  'style' => 'seamless',
  'label_placement' => 'top',
  'instruction_placement' => 'label',
  'hide_on_screen' => array (
    0 => 'the_content',
    1 => 'excerpt',
    2 => 'custom_fields',
    3 => 'discussion',
    4 => 'comments',
    5 => 'revisions',
  ),
));


register_field_group(array (
  'key' => 'group_53d77faf22168',
  'title' => 'Business Fields',
  'fields' => array (
    array (
      'key' => 'field_2',
      'label' => 'URL',
      'name' => 'townscape_url',
      'prefix' => '',
      'type' => 'text',
      'instructions' => 'Link to the business\'s website.',
      'required' => 0,
      'conditional_logic' => 0,
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'formatting' => 'none',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_4',
      'label' => 'Address, Line1',
      'name' => 'townscape_address1',
      'prefix' => '',
      'type' => 'text',
      'instructions' => 'The street address of the Business',
      'required' => 0,
      'conditional_logic' => 0,
      'default_value' => '1234 Street Ave',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'formatting' => 'none',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_14',
      'label' => 'Address, Line 2',
      'name' => 'townscape_address2',
      'prefix' => '',
      'type' => 'text',
      'instructions' => 'The town the business is located in.',
      'required' => 0,
      'conditional_logic' => 0,
      'default_value' => $town_name,
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'formatting' => 'none',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_3',
      'label' => 'Phone',
      'name' => 'townscape_phone',
      'prefix' => '',
      'type' => 'text',
      'instructions' => 'The phone number of the business',
      'required' => 0,
      'conditional_logic' => 0,
      'default_value' => '+353-(0)27-0000',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'formatting' => 'none',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_16',
      'label' => 'Contact Email',
      'name' => 'townscape_email',
      'prefix' => '',
      'type' => 'text',
      'instructions' => 'Business contact email.',
      'required' => 0,
      'conditional_logic' => 0,
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'formatting' => 'none',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
  ),
  'location' => array (
    array (
      array (
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'business',
      ),
    ),
  ),
  'menu_order' => 0,
  'position' => 'normal',
  'style' => 'seamless',
  'label_placement' => 'top',
  'instruction_placement' => 'label',
  'hide_on_screen' => array (
    0 => 'the_content',
    1 => 'excerpt',
    2 => 'custom_fields',
    3 => 'discussion',
    4 => 'comments',
    5 => 'revisions',
    6 => 'slug',
    7 => 'format',
    8 => 'tags',
    9 => 'send-trackbacks',
  ),
));

register_field_group(array (
  'key' => 'group_53d77faf47ac9',
  'title' => 'Gem Fields',
  'fields' => array (
    array (
      'key' => 'field_538c165ad3666',
      'label' => 'Related Content',
      'name' => 'townscape_related_content',
      'prefix' => '',
      'type' => 'relationship',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'return_format' => 'id',
      'post_type' => array (
      ),
      'taxonomy' => array (
      ),
      'filters' => array (
        0 => 'search',
        1 => 'post_type',
      ),
      'max' => 3,
      'elements' => array (
        0 => 'featured_image',
        1 => 'post_type',
      ),
    ),
  ),
  'location' => array (
    array (
      array (
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'gem',
      ),
    ),
  ),
  'menu_order' => 0,
  'position' => 'normal',
  'style' => 'seamless',
  'label_placement' => 'top',
  'instruction_placement' => 'label',
  'hide_on_screen' => array (
  ),
));


register_field_group(array (
  'key' => 'group_53d77faf66e2b',
  'title' => 'Options Page',
  'fields' => array (
    array (
      'key' => 'field_539d503269d86',
      'label' => 'Site GPS',
      'name' => 'townscape_site_gps',
      'prefix' => '',
      'type' => 'google_map',
      'instructions' => '',
      'required' => 1,
      'conditional_logic' => 0,
      'center_lat' => '53.3649212',
      'center_lng' => '-7.8266918',
      'zoom' => 7,
      'height' => '',
    ),
    array (
      'key' => 'field_53b7d795fde61',
      'label' => 'Town Name',
      'name' => 'townscape_site_town',
      'prefix' => '',
      'type' => 'text',
      'instructions' => 'eg. Bantry or Los Angeles',
      'required' => 0,
      'conditional_logic' => 0,
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'formatting' => 'html',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_53b7d7b5fde62',
      'label' => 'Town County/State',
      'name' => 'townscape_site_state',
      'prefix' => '',
      'type' => 'text',
      'instructions' => 'eg. Co. Cork, or CA',
      'required' => 0,
      'conditional_logic' => 0,
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'formatting' => 'html',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_53b7d7d1fde63',
      'label' => 'Town Country',
      'name' => 'townscape_site_country',
      'prefix' => '',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'formatting' => 'html',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_53b7d800fde64',
      'label' => 'Town Logo',
      'name' => 'townscape_site_logo',
      'prefix' => '',
      'type' => 'image',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'preview_size' => 'large',
      'library' => 'all',
      'return_format' => 'url',
    ),
    array (
      'key' => 'field_53c3cd333d495',
      'label' => 'Site Booking.com Affiliate ID',
      'name' => 'townscape_site_booking_com_affiliate_id',
      'prefix' => '',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'formatting' => 'html',
      'maxlength' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
  ),
  'location' => array (
    array (
      array (
        'param' => 'options_page',
        'operator' => '==',
        'value' => 'site-settings',
      ),
    ),
  ),
  'menu_order' => 0,
  'position' => 'normal',
  'style' => 'seamless',
  'label_placement' => 'top',
  'instruction_placement' => 'label',
  'hide_on_screen' => array (
  ),
));



register_field_group(array (
  'key' => 'group_53d77faf940b8',
  'title' => 'Physical Place Fields',
  'fields' => array (
    array (
      'key' => 'field_539ebd61413bf',
      'label' => 'GPS Latitude',
      'name' => 'townscape_gps_lat',
      'prefix' => '',
      'type' => 'number',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'min' => '',
      'max' => '',
      'step' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    array (
      'key' => 'field_539ebd8d413c0',
      'label' => 'GPS Longitude',
      'name' => 'townscape_gps_lng',
      'prefix' => '',
      'type' => 'number',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'min' => '',
      'max' => '',
      'step' => '',
      'readonly' => 0,
      'disabled' => 0,
    ),
    /** array (
      'key' => 'field_539ec81be44ee',
      'label' => 'Wheelchair Access',
      'name' => 'townscape_wheelchair',
      'prefix' => '',
      'type' => 'radio',
      'instructions' => 'Does this place have wheelchair access?',
      'required' => 1,
      'conditional_logic' => 0,
      'choices' => array (
        'yes' => 'Yes',
        'no' => 'No',
        'ignore' => 'Not Applicable',
      ),
      'other_choice' => 0,
      'save_other_choice' => 0,
      'default_value' => '',
      'layout' => 'vertical',
    ),
    array (
      'key' => 'field_539fcadcb692e',
      'label' => 'Wifi Available',
      'name' => 'townscape_wifi',
      'prefix' => '',
      'type' => 'radio',
      'instructions' => 'Does this place have wifi?',
      'required' => 1,
      'conditional_logic' => 0,
      'choices' => array (
        'yes' => 'Yes',
        'no' => 'No',
        'ignore' => 'Not Applicable',
      ),
      'other_choice' => 0,
      'save_other_choice' => 0,
      'default_value' => '',
      'layout' => 'vertical',
    ), **/
  ),
  'location' => array (
    array (
      array (
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'business',
      ),
    ),
    array (
      array (
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'gem',
      ),
    ),
  ),
  'menu_order' => 0,
  'position' => 'normal',
  'style' => 'default',
  'label_placement' => 'top',
  'instruction_placement' => 'label',
  'hide_on_screen' => array (
  ),
));

register_field_group(array (
  'key' => 'group_53d77fafb0727',
  'title' => 'Very Common Fields',
  'fields' => array (
    array (
      'key' => 'field_5368cbf9f252a',
      'label' => 'Subtitle',
      'name' => 'townscape_subtitle',
      'prefix' => '',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'formatting' => 'html',
      'maxlength' => 90,
      'readonly' => 0,
      'disabled' => 0,
    ),

    array (
      'key' => 'field_539ed5e9fcb4c',
      'label' => 'Short Description',
      'name' => 'townscape_short_description',
      'prefix' => '',
      'type' => 'textarea',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'default_value' => '',
      'placeholder' => '',
      'maxlength' => 140,
      'rows' => '',
      'new_lines' => 'br',
      'readonly' => 0,
      'disabled' => 0,
    ),
  ),
  'location' => array (
    array (
      array (
        'param' => 'taxonomy',
        'operator' => '==',
        'value' => 'business_type',
      ),
    ),
    array (
      array (
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'page',
      ),
    ),
    array (
      array (
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'business',
      ),
    ),
    array (
      array (
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'gem',
      ),
    ),
  ),
  'menu_order' => 0,
  'position' => 'acf_after_title',
  'style' => 'seamless',
  'label_placement' => 'top',
  'instruction_placement' => 'label',
  'hide_on_screen' => array (
  ),
));

register_field_group(array (
  'key' => 'group_53d77fafcc625',
  'title' => 'Amenities - Accommodation',
  'fields' => array (
    array (
      'key' => 'field_5368a5c9ab0c2',
      'label' => 'Amenities',
      'name' => 'townscape_amenities',
      'prefix' => '',
      'type' => 'checkbox',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'choices' => array (
        'wifi' => 'Wifi',
        'leisure' => 'Leisure Centre/Spa',
        'parking' => 'Onsite Parking',
        'handicap' => 'Handicap Accessible',
        'breakfast' => 'Breakfast Included',
        'coffee' => 'Tea/Coffee Facilities',
        'concierge' => 'Concierge Services',
        'iron' => 'Iron/Ironing Board',
      ),
      'default_value' => '',
      'layout' => 'horizontal',
    ),
  ),
  'location' => array (
    array (
      array (
        'param' => 'post_taxonomy',
        'operator' => '==',
        'value' => 'business_type:accommodation',
      ),
    ),
    array (
      array (
        'param' => 'post_taxonomy',
        'operator' => '==',
        'value' => 'business_type:hotels-kenmare',
      ),
    ),
  ),
  'menu_order' => 1,
  'position' => 'normal',
  'style' => 'seamless',
  'label_placement' => 'top',
  'instruction_placement' => 'label',
  'hide_on_screen' => array (
    0 => 'the_content',
  ),
));

endif;
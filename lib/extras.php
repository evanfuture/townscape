<?php
/**
Include ACF Fields
**/


include_once('acf-fields.php');
if( function_exists('acf_add_options_page') ) {
  acf_add_options_page(array(
    'page_title'  => 'Site Settings',
    'menu_title'  => 'Site Settings',
    'menu_slug'   => 'site-settings',
    'capability'  => 'edit_themes',
  ));
}

/**
 Clean up the_excerpt()
**/
function roots_excerpt_more($more) {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'roots') . '</a>';
}
add_filter('excerpt_more', 'roots_excerpt_more');

/**
 Manage output of wp_title()
 */
function roots_wp_title($title) {
  if (is_feed()) {
    return $title;
  }

  $title .= get_bloginfo('name');

  return $title;
}
add_filter('wp_title', 'roots_wp_title', 10);


/**
Custom Breadcrumbs
**/
function townscape_breadcrumbs() {

  $showOnHome = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
  $delimiter = '&raquo;'; // delimiter between crumbs
  $home = '<span class="icon-home"></span>Home'; // text for the 'Home' link
  $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
  $before = '<span class="active">'; // tag before the current crumb
  $after = '</span>'; // tag after the current crumb

  global $post;
  $homeLink = get_bloginfo('url');

  if (is_home() || is_front_page()) {

    if ($showOnHome == 1) echo '<a href="' . $homeLink . '">' . $home . '</a>';

  } else {

    echo '<a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ';

    if ( is_category() ) {
      $thisCat = get_category(get_query_var('cat'), false);
      if ($thisCat->parent != 0) echo get_category_parents($thisCat->parent, TRUE, ' ' . $delimiter . ' ');
      echo $before . 'Archive by category "' . single_cat_title('', false) . '"' . $after;

    } elseif ( is_tax('business_type')) {
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$parent = get_term($term->parent, get_query_var('taxonomy') );
		if ($term->parent != 0) echo '<a href="/' . $parent->slug . '">' . $parent->name . '</a> ' . $delimiter . ' ';
		echo '<a href="/' . $term->slug . '" class="active">' . $term->name . '</a>';

	} elseif ( is_search() ) {
      echo $before . 'Search results for "' . get_search_query() . '"' . $after;

    } elseif ( is_day() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('d') . $after;

    } elseif ( is_month() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('F') . $after;

    } elseif ( is_year() ) {
      echo $before . get_the_time('Y') . $after;

    } elseif ( is_single() && !is_attachment() ) {
      if ( get_post_type() == 'business' ) {
        $post_type = get_post_type_object(get_post_type());
        $terms = get_the_terms( $post->ID, 'business_type' );
        if ( $terms && ! is_wp_error( $terms ) ) {
          $tax_links = array();
          foreach ( $terms as $term ) {
            $tax_links[] = $term->name;
            $out[] =
            '  <a href="'
          .    get_term_link( $term->slug, 'business_type' ) .'">'
          .    $term->name
          . "</a>\n";
          }
          $tax = join( " / ", $out );
          $slug = $post_type->rewrite;
          echo $tax;
        }
        if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
      } elseif ( get_post_type() == 'gem' ) {
        $post_type = get_post_type_object(get_post_type());
        $terms = get_the_terms( $post->ID, 'gem_type' );
        if ( $terms && ! is_wp_error( $terms ) ) {
          $tax_links = array();
          foreach ( $terms as $term ) {
            $tax_links[] = $term->name;
            $out[] =
            '  <a href="'
          .    get_term_link( $term->slug, 'gem_type' ) .'">'
          .    $term->name
          . "</a>\n";
          }
          $tax = join( " / ", $out );
          $slug = $post_type->rewrite;
          echo $tax;
        }
        if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
      } elseif ( get_post_type() == 'product' ) {
        $post_type = get_post_type_object(get_post_type());
        $terms = get_the_terms( $post->ID, 'product_cat' );
        if ( $terms && ! is_wp_error( $terms ) ) {
          $tax_links = array();
          foreach ( $terms as $term ) {
            $tax_links[] = $term->name;
            $out[] =
            '  <a href="'
          .    get_term_link( $term->slug, 'product_cat' ) .'">'
          .    $term->name
          . "</a>\n";
          }
          $tax = join( " / ", $out );
          $slug = $post_type->rewrite;
          echo $tax;
        }
        if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
      } else {
        $cat = get_the_category();
        $cat = $cat[0];
        $cats = get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
        if ($showCurrent == 0) $cats = preg_replace("#^(.+)\s$delimiter\s$#", "$1", $cats);
        echo $cats;
        if ($showCurrent == 1) echo $before . get_the_title() . $after;
      }

    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
      $post_type = get_post_type_object(get_post_type());
      echo $before . $post_type->labels->singular_name . $after;

    } elseif ( is_attachment() ) {
      $attachment_id = get_the_ID(); // attachment ID
      $attachment_meta = wp_get_attachment($attachment_id);
      echo $attachment_meta['title'] . '.jpg';


    } elseif ( is_page() && !$post->post_parent ) {
      if ($showCurrent == 1) echo $before . get_the_title() . $after;

    } elseif ( is_page() && $post->post_parent ) {
      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      for ($i = 0; $i < count($breadcrumbs); $i++) {
        echo $breadcrumbs[$i];
        if ($i != count($breadcrumbs)-1) echo ' ' . $delimiter . ' ';
      }
      if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;

    } elseif ( is_tag() ) {
      echo $before . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;

    } elseif ( is_author() ) {
       global $author;
      $userdata = get_userdata($author);
      echo $before . 'Articles posted by ' . $userdata->display_name . $after;

    } elseif ( is_404() ) {
      echo $before . '404 Error - Page Not Found' . $after;
    }

    if ( get_query_var('paged') ) {
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
      echo __('Page') . ' ' . get_query_var('paged');
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
    }

  }
} // end townscape_breadcrumbs()


/**
Change Editor Style HTML
**/

function townscape_add_editor_styles() {
    add_editor_style( 'custom-editor-style.css' );
}
add_action( 'init', 'townscape_add_editor_styles' );


/**
Custom nav walker
**/

class townscape_simple_walker extends Walker
{
    public function walk( $elements, $max_depth )
    {
        $list = array ();

        foreach ( $elements as $item )
          $list[] = "<a href='$item->url'><span class='" . $item->classes[0] . "'></span>$item->title</a>";

        return join( "\n", $list );
    }
}


/**
Get All Attachment Metadata
  Custom code from: http://wordpress.stackexchange.com/questions/125554/get-image-description
**/
function wp_get_attachment( $attachment_id ) {

$attachment = get_post( $attachment_id );
return array(
    'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
    'caption' => $attachment->post_excerpt,
    'description' => $attachment->post_content,
    'href' => get_permalink( $attachment->ID ),
    'src' => $attachment->guid,
    'title' => $attachment->post_title
);
}

/**
Alter FacetWp's sorting options
**/
// Example: remove the Date (Oldest) option
function my_facetwp_sort_options( $options, $params ) {
    unset( $options['date_asc'] );
    unset( $options['date_desc'] );
    return $options;
}

add_filter( 'facetwp_sort_options', 'my_facetwp_sort_options', 10, 2 );

/**
Always Add Booking.com Affiliate ID
**/
function townscape_booking_com_affiliate_add( $townscape_booking_com_url ) {

  $url_host = parse_url($townscape_booking_com_url, PHP_URL_HOST);
  $url_path = parse_url($townscape_booking_com_url, PHP_URL_PATH);
  $aid = get_field('townscape_site_booking_com_affiliate_id', 'option');
  $affiliate_url = 'http://';
  $affiliate_url .= $url_host;
  $affiliate_url .= $url_path;
  $affiliate_url .= '?aid=' . $aid;
  return $affiliate_url;
}


/**
Progress Map - Load only on Front Page
**/
function cspm_remove_style_files(){

  if (!class_exists("CodespacingProgressMap"))
    return;

  $ProgressMapClass = CodespacingProgressMap::this();

  if($ProgressMapClass->loading_scripts == "only_pages"){

    global $post;

    $IDs = array_merge(
          explode(",", str_replace(" ", "", $ProgressMapClass->load_on_page_ids)),
          explode(",", str_replace(" ", "", $ProgressMapClass->load_on_post_ids))
         );

    $page_templates = explode(",", str_replace(" ", "", $ProgressMapClass->load_on_page_templates));
    $current_template_name = basename(get_page_template());

    if($ProgressMapClass->include_or_remove_option == "include"){
      if(!in_array($post->ID, $IDs)) $ProgressMapClass->cspm_deregister_styles();
      if(in_array($current_template_name, $page_templates)) $ProgressMapClass->cspm_styles();
    }else{
      if(in_array($post->ID, $IDs) || in_array($current_template_name, $page_templates))
        $ProgressMapClass->cspm_deregister_styles();
    }

  }

}

add_action("wp_print_styles", "cspm_remove_style_files", 100);

function cspm_remove_script_files(){

  if (!class_exists("CodespacingProgressMap"))
    return;

  $ProgressMapClass = CodespacingProgressMap::this();

  if($ProgressMapClass->loading_scripts == "only_pages"){

    global $post;

    $IDs = array_merge(
          explode(",", str_replace(" ", "", $ProgressMapClass->load_on_page_ids)),
          explode(",", str_replace(" ", "", $ProgressMapClass->load_on_post_ids))
         );

    $page_templates = explode(",", str_replace(" ", "", $ProgressMapClass->load_on_page_templates));
    $current_template_name = basename(get_page_template());

    if($ProgressMapClass->include_or_remove_option == "include"){
      if(!in_array($post->ID, $IDs)) $ProgressMapClass->cspm_deregister_scripts();
      if(in_array($current_template_name, $page_templates)) $ProgressMapClass->cspm_scripts();
    }else{
      if(in_array($post->ID, $IDs) || in_array($current_template_name, $page_templates))
        $ProgressMapClass->cspm_deregister_scripts();
    }

  }

}

add_action("wp_print_scripts", "cspm_remove_script_files", 100);

/**
GPS Distance Calculator
**/

function townscape_distance($lat1, $lon1, $lat2, $lon2, $unit) {
  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);
  if ($unit == "K") {
    return ($miles * 1.609344);
    } else {
        return $miles;
      }
}

/**
Sort Business Listings by Name
**/

function townscape_alpha_display( $query ) {

    if ( is_archive() ) {
        $query->set( 'orderby', 'name' );
        $query->set( 'order', 'ASC' );
        return;
    }
}
add_action( 'pre_get_posts', 'townscape_alpha_display', 1 );

/**
Add Connections between Listings and Sprout Clients
**/
function my_connection_types() {
    p2p_register_connection_type( array(
        'name' => 'business_to_client',
        'from' => 'sa_client',
        'to' => 'business',
        'cardinality' => 'one-to-many'
    ) );
    p2p_register_connection_type( array(
        'name' => 'business_to_invoice',
        'from' => 'sa_invoice',
        'to' => 'business',
        'cardinality' => 'one-to-many'
    ) );
    p2p_register_connection_type( array(
        'name' => 'business_to_surl',
        'from' => 'surl',
        'to' => 'business',
        'cardinality' => 'one-to-one'
    ) );

}
add_action( 'p2p_init', 'my_connection_types' );

function append_date_to_candidate_title( $title, $post, $ctype ) {
    if ( 'business_to_invoice' == $ctype->name && 'sa_invoice' == $post->post_type ) {
        $title .= " (" . $post->_wp_page_template . ")";
    }

    return $title;
}

add_filter( 'p2p_candidate_title', 'append_date_to_candidate_title', 10, 3 );

/**
Hide Menu Items for non-admins
**/
function ts_remove_menu_items() {
    if( !current_user_can( 'administrator' ) ):
      remove_menu_page( 'edit.php?post_type=surl' );
      remove_menu_page( 'edit.php?post_type=business' );
      remove_menu_page( 'edit.php?post_type=gem' );
      remove_menu_page( 'edit.php?post_type=sa_invoice' );
      remove_menu_page( 'edit.php?post_type=sa_estimate' );
      remove_menu_page('tools.php');
      remove_menu_page('acf-options');
    endif;
}
add_action( 'admin_menu', 'ts_remove_menu_items' );

/**
Fix Sprout Invoice Missing currency symbol
**/

function set_euros_localeconv( $locale = array() ) {
  $locale = array(
    'decimal_point' => '.',
    'thousands_sep' => '',
    'int_curr_symbol' => 'EUR',
    'currency_symbol' => '€',
    'mon_decimal_point' => '.',
    'mon_thousands_sep' =>  ',',
    'positive_sign' => '',
    'negative_sign' => '-',
    'int_frac_digits' => 2,
    'frac_digits' => 2,
    'p_cs_precedes' => 1,
    'p_sep_by_space' => 0,
    'n_cs_precedes' => 1,
    'n_sep_by_space' => 0,
    'p_sign_posn' => 1,
    'n_sign_posn' => 1,
    'grouping' => array(),
    'mon_grouping' => array( 3, 3 ),
  );
  return $locale;
};
add_filter( 'si_localeconv', 'set_euros_localeconv' );

/**
Guidebook Downloads
 */

function guidebook_locker() {
  global $post;
  $cookie_name = "kenmare_guidebook";
  $cookie_value = "their email";
  setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/", "kenmare.com"); // 86400 = 1 day
  if(!isset($_COOKIE[$cookie_name])) {
    $form = '<div class="et_bloom_locked_container et_bloom_optin_1" data-page_id="2" data-optin_id="optin_1" data-list_id="mailchimp_1364846aa9">

					<div class="et_bloom_locked_form">
						<div class="et_bloom_inline_form et_bloom_optin et_bloom_optin_1">
				<style type="text/css">.et_bloom .et_bloom_optin_1 .et_bloom_form_content { background-color: #e5e0da !important; } .et_bloom .et_bloom_optin_1 .et_bloom_form_container .et_bloom_form_header { background-color: #64ba70 !important; } .et_bloom .et_bloom_optin_1 .curve_edge .curve { fill: #64ba70} .et_bloom .et_bloom_optin_1 .et_bloom_form_content button { background-color: #445a6d !important; } .et_bloom .et_bloom_optin_1 .et_bloom_border_solid { border-color: #445a6d !important } .et_bloom .et_bloom_optin_1 .et_bloom_form_content button { background-color: #445a6d !important; } .et_bloom .et_bloom_optin_1 h2, .et_bloom .et_bloom_optin_1 h2 span, .et_bloom .et_bloom_optin_1 h2 strong { font-family: "Open Sans", Helvetica, Arial, Lucida, sans-serif; }.et_bloom .et_bloom_optin_1 p, .et_bloom .et_bloom_optin_1 p span, .et_bloom .et_bloom_optin_1 p strong, .et_bloom .et_bloom_optin_1 form input, .et_bloom .et_bloom_optin_1 form button span { font-family: "Open Sans", Helvetica, Arial, Lucida, sans-serif; } </style>
				<div class="et_bloom_form_container et_bloom_popup_container with_edge curve_edge et_bloom_border_solid et_bloom_rounded et_bloom_form_text_dark et_bloom_form_bottom et_bloom_inline_1_field">

			<div class="et_bloom_form_container_wrapper clearfix">
				<div class="et_bloom_header_outer">
					<div class="et_bloom_form_header split et_bloom_header_text_light">
						<img width="500" height="363" src="http://1rsi1s39q3co2gd3ll1morj2.wpengine.netdna-cdn.com/wp-content/uploads/book.png" class=" et_bloom_image_slideup et_bloom_image" alt="book">
						<div class="et_bloom_form_text">
						<h2>Download Our Guidebook</h2><p><span style="color: rgba(0, 0, 0, 0.498039); font-family: \'Open Sans\', Helvetica, Arial, Lucida, sans-serif; font-size: 14px; letter-spacing: 0.159999996423721px; line-height: 22.3999996185303px;">Join our mailing list to receive your free pdf copy of the Kenmare Guide, a 64 page booklet with articles and listings covering the greater Kenmare Area.</span></p>
					</div>

					</div>
				</div>
				<div class="et_bloom_form_content et_bloom_1_field et_bloom_bottom_inline">

					<svg class="curve et_bloom_default_edge" xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="20" viewBox="0 0 100 100" preserveAspectRatio="none">
						<path d="M0 0 C40 100 60 100 100 0 Z"></path>
					</svg>
					<form method="post" class="clearfix">

						<p class="et_bloom_popup_input et_bloom_subscribe_email">
							<input placeholder="Email">
						</p>
						<button data-optin_id="optin_1" data-service="mailchimp" data-list_id="1364846aa9" data-page_id="2" data-account="kenmarecreative" class="et_bloom_submit_subscription">
							<span class="et_bloom_subscribe_loader"></span>
							<span class="et_bloom_button_text et_bloom_button_text_color_light">Request Download</span>
						</button>
					</form>
					<div class="et_bloom_success_container">
						<span class="et_bloom_success_checkmark"></span>
					</div>
					<h2 class="et_bloom_success_message">Thank You!</h2>

				</div>
			</div>
			<span class="et_bloom_close_button"></span>
				</div>
			</div>
					</div>
				</div>';
        echo $form;
  } else {
      $download = '<div class="et_bloom_form_container et_bloom_popup_container with_edge curve_edge et_bloom_border_solid et_bloom_rounded et_bloom_form_text_dark et_bloom_form_bottom et_bloom_inline_1_field" style="max-width:800px">
    	  <div class="et_bloom_form_container_wrapper clearfix">
    	    <div class="et_bloom_header_outer">
    	      <div class="et_bloom_form_header split et_bloom_header_text_light">
    	        <img width="500" height="363" src="'.$upload_dir['baseurl'].'/book.png" class="et_bloom_image2" alt="book">
    	        <div class="et_bloom_form_text">
    		  <h3>Thanks for Signing Up!</h3>
    	          <p><span style="color: rgba(0, 0, 0, 0.498039); font-family: \'Open Sans\', Helvetica, Arial, Lucida, sans-serif; font-size: 14px; letter-spacing: 0.159999996423721px; line-height: 22.3999996185303px;">You\'ll receive an occasional newsletter from us on what\'s happening in Kenmare.  In the meantime, click the button to download your copy of the Kenmare Guide, or scroll down to view it right away.</span></p>
    		</div>
    	      </div>
    	    </div>
    	    <div class="et_bloom_form_content et_bloom_1_field et_bloom_bottom_inline" style="text-align:center;">
    	      <svg class="curve et_bloom_default_edge" xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="20" viewBox="0 0 100 100" preserveAspectRatio="none"><path d="M0 0 C40 100 60 100 100 0 Z"></path></svg>
    	      <a href="'.home_url().'/KenmareGuide2015.pdf" alt="Kenmare Guide pdf" target="_blank" style="background-color:#445a6d;border-radius:3px;padding:10px;font-size:14px;color:#fff;">Download Now</a>
    				<p style="margin-top:20px;">KenmareGuide2015.pdf (4.2Mb)</p>
    	    </div>
    	  </div>
    	</div>';
      echo $download;
  }
}

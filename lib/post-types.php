<?php

/**
Businesses
 */

add_action( 'after_setup_theme', 'business_cpt' );
function business_cpt() {
    if ( ! class_exists( 'Super_Custom_Post_Type' ) )
        return;
    $businesses = new Super_Custom_Post_Type( 'business', 'Business', 'Businesses' );
    $business_types = new Super_Custom_Taxonomy( 'business_type', 'Business Type', 'Business Types', 'category' );
    $businesses->set_icon( 'tree_conifer' );
    connect_types_and_taxes( $businesses, $business_types);
}

add_filter( 'manage_edit-business_columns', 'business_columns' ) ;
function business_columns( $columns ) {

	$columns = array(
		'cb' => '<input type="checkbox" />',
		'riv_post_thumbs' => __('Thumbs'),
		'title' => __( 'Business Name' ),
		'business_type' => __( 'Business Type' ),
		'townscape_gps_lat' => __( 'Distance from Town' ),
		'date' => __( 'Date' ),
		'expirationdate' => __('Expires')

	);

	return $columns;
}

add_action( 'manage_business_posts_custom_column', 'manage_business_columns', 10, 2 );

function manage_business_columns( $column, $post_id ) {
	global $post;
	global $wpdb;
	switch( $column ) {
		/* If displaying the 'duration' column. */
		case 'riv_post_thumbs' :
			echo the_post_thumbnail( 'townscape_thumb' );
			break;
		case 'townscape_gps_lat' :
			/* Get the post meta. */
			$townscape_gps_lat = get_post_meta( $post_id, 'townscape_gps_lat', true );
			$townscape_gps_lng = get_post_meta( $post_id, 'townscape_gps_lng', true );
			$site_lat = get_field('townscape_site_gps_lat', 'option');
			$site_lng = get_field('townscape_site_gps_lng', 'option');

			/* If no location is found, output a default message. */
			if ( empty( $townscape_gps_lat ) ) {
				echo __( 'Unknown' );
			}

			else {
				$distance = townscape_distance($townscape_gps_lat, $townscape_gps_lng, $site_lat, $site_lng, "K");
				$rounded_distance = ceil($distance);
				if($rounded_distance <=1 ){echo 'In Town';} else {
				echo $rounded_distance . " km<br>";
				}
			}

			break;

		/* If displaying the 'genre' column. */
		case 'business_type' :

			/* Get the genres for the post. */
			$terms = get_the_terms( $post_id, 'business_type' );

			/* If terms were found. */
			if ( !empty( $terms ) ) {

				$out = array();

				/* Loop through each term, linking to the 'edit posts' page for the specific term. */
				foreach ( $terms as $term ) {
					$out[] = sprintf( '<a href="%s">%s</a>',
						esc_url( add_query_arg( array( 'post_type' => $post->post_type, 'business_type' => $term->slug ), 'edit.php' ) ),
						esc_html( sanitize_term_field( 'name', $term->name, $term->term_id, 'business_type', 'display' ) )
					);
				}

				/* Join the terms, separating them with a comma. */
				echo join( ', ', $out );
			}

			/* If no terms were found, output a default message. */
			else {
				_e( 'No Types' );
			}

			break;

		/* Just break out of the switch statement for everything else. */
		default :
			break;
	}
}

add_filter( 'manage_edit-business_sortable_columns', 'business_sortable_columns' );

function business_sortable_columns( $columns ) {

	$columns['townscape_distance'] = 'townscape_distance';

	return $columns;
}
/* Only run our customization on the 'edit.php' page in the admin. */
add_action( 'load-edit.php', 'business_edit_load' );

function business_edit_load() {
	add_filter( 'request', 'business_sort' );
}

/* Sorts the movies. */
function business_sort( $vars ) {

	/* Check if we're viewing the 'movie' post type. */
	if ( isset( $vars['post_type'] ) && 'business' == $vars['post_type'] ) {

		/* Check if 'orderby' is set to 'location'. */
		if ( isset( $vars['orderby'] ) && 'townscape_distance' == $vars['orderby'] ) {

			/* Merge the query vars with our custom variables. */
			$vars = array_merge(
				$vars,
				array(
					'meta_key' => 'townscape_distance',
					'orderby' => 'meta_value'
				)
			);
		}
	}

	return $vars;
}


/**
Gems
 */

add_action( 'after_setup_theme', 'gem_cpt' );
function gem_cpt() {
    if ( ! class_exists( 'Super_Custom_Post_Type' ) )
        return;
    $gems = new Super_Custom_Post_Type( 'gem', 'Gem', 'Gems' );
    $gem_types = new Super_Custom_Taxonomy( 'gem_type', 'Gem Type', 'Gem Types', 'category' );
    $gems->set_icon( 'tree_conifer' );
    connect_types_and_taxes( $gems, $gem_types);
}


add_filter( 'manage_edit-gem_columns', 'gem_columns' ) ;
function gem_columns( $columns ) {

	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __( 'Gem Name' ),
		'gem_type' => __( 'Gem Type' ),
		'townscape_gps' => __( 'GPS' ),
		'date' => __( 'Date' )
	);

	return $columns;
}

add_action( 'manage_gem_posts_custom_column', 'manage_gem_columns', 10, 2 );

function manage_gem_columns( $column, $post_id ) {
	global $post;
	global $wpdb;
	switch( $column ) {
		/* If displaying the 'duration' column. */
		case 'townscape_distance' :
			/* Get the post meta. */
			$townscape_distance = get_post_meta( $post_id, 'townscape_distance', true );

			/* If no location is found, output a default message. */
			if ( empty( $townscape_distance ) )
				echo __( 'Unknown' );

			/* If there is a townscape_distance, append 'minutes' to the text string. */
			else
				printf( $townscape_distance );

			break;

		/* If displaying the 'genre' column. */
		case 'gem_type' :

			/* Get the genres for the post. */
			$terms = get_the_terms( $post_id, 'gem_type' );

			/* If terms were found. */
			if ( !empty( $terms ) ) {

				$out = array();

				/* Loop through each term, linking to the 'edit posts' page for the specific term. */
				foreach ( $terms as $term ) {
					$out[] = sprintf( '<a href="%s">%s</a>',
						esc_url( add_query_arg( array( 'post_type' => $post->post_type, 'gem_type' => $term->slug ), 'edit.php' ) ),
						esc_html( sanitize_term_field( 'name', $term->name, $term->term_id, 'gem_type', 'display' ) )
					);
				}

				/* Join the terms, separating them with a comma. */
				echo join( ', ', $out );
			}

			/* If no terms were found, output a default message. */
			else {
				_e( 'No Types' );
			}

			break;

		/* Just break out of the switch statement for everything else. */
		default :
			break;
	}
}

add_filter( 'manage_edit-gem_sortable_columns', 'gem_sortable_columns' );

function gem_sortable_columns( $columns ) {

	$columns['townscape_distance'] = 'townscape_distance';

	return $columns;
}
/* Only run our customization on the 'edit.php' page in the admin. */
add_action( 'load-edit.php', 'gem_edit_load' );

function gem_edit_load() {
	add_filter( 'request', 'gem_sort' );
}

/* Sorts the movies. */
function gem_sort( $vars ) {

	/* Check if we're viewing the 'movie' post type. */
	if ( isset( $vars['post_type'] ) && 'gem' == $vars['post_type'] ) {

		/* Check if 'orderby' is set to 'location'. */
		if ( isset( $vars['orderby'] ) && 'townscape_gps' == $vars['orderby'] ) {

			/* Merge the query vars with our custom variables. */
			$vars = array_merge(
				$vars,
				array(
					'meta_key' => 'townscape_gps',
					'orderby' => 'meta_value'
				)
			);
		}
	}

	return $vars;
}
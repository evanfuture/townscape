<?php
/**
  Theme initial setup and constants
 */
function roots_setup() {
  // Make theme available for translation
  // Community translations can be found at https://github.com/roots/roots-translations
  load_theme_textdomain('roots', get_template_directory() . '/lang');

  // Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus
  register_nav_menus(array(
    'primary_navigation' => __('Primary Navigation', 'roots'),
    'button_navigation' => __('Button Navigation', 'roots'),
    'sitemap_navigation' => __('Sitemap Navigation', 'roots')
  ));

  // Add post thumbnails
  // http://codex.wordpress.org/Post_Thumbnails
  // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
  // http://codex.wordpress.org/Function_Reference/add_image_size
  add_theme_support('post-thumbnails');
    add_image_size('townscape_header'   , 800, 387, true);
    add_image_size('townscape_thumb'    , 200, 130, true);
    add_image_size('townscape_gallery'  , 300, 200, true);
    add_image_size('townscape_logo'     , 9999, 70);
    add_image_size('townscape_logo_sm'  , 9999, 30);
    add_image_size('townscape_full'     , 860, 9999);
    add_image_size('townscape_listing'  , 350, 9999);

  // Tell the TinyMCE editor to use a custom stylesheet
  add_editor_style('/assets/css/editor-style.css');

}
add_action('after_setup_theme', 'roots_setup');
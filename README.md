# [Townscape Theme](http://kenmarecreative.com/townscape)

Townscape is a Wordpress theme for building a tourism website.

* Source: [https://github.com/evanfuture/townscape](https://github.com/evanfuture/townscape)
* Documentation: Forthcoming

## Features

* Organized file and template structure
* ARIA roles and microformats
* Cleaner HTML output of navigation menus
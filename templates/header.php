<header class="section site-header" role="banner">
  <div class="site-logo">
  <?php
    $town = get_field('townscape_site_town', 'option');
    $state = get_field('townscape_site_state', 'option');
    $country = get_field('townscape_site_country', 'option');
    $logo = get_field('townscape_site_logo', 'option');
  ?>
    <img alt="<?php echo $town;?>, <?php echo $state;?>, <?php echo $country;?>" src="<?php echo $logo;?>"/>
    <h1><a class="brand" href="<?php echo home_url('/') ?>"><?php echo $town;?><span>, <?php echo $state;?></span></a></h1>
  </div>
  <nav class="nav-main" role="navigation">
  <label for="show-menu" class="show-menu">Show Menu</label>
  <input type="checkbox" id="show-menu" role="button">
  <ul id="main-menu" class="main-menu">
    <?php
          if (has_nav_menu('primary_navigation')) :
        $menuParameters = array(
          'theme_location' => 'primary_navigation',
          'echo'            => false,
          'items_wrap' => '%3$s',
        );

        echo wp_nav_menu( $menuParameters );

      endif;
    ?>

    <li><a href="#"><span class="icon-search"></span></a></li>
    </ul>
    <script>
      var menu = new cbpTooltipMenu( document.getElementById( 'main-menu' ) );
    </script>
  </nav>
</header><!--/site-header-->
<div class="section search" id="search">
<?php get_search_form(); ?>
</div>

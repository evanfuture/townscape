<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php wp_title('|', true, 'right'); ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1">

<?php if (is_singular('business')) {?>

  <?php $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'townscape_listing' );?>

  <meta name="description" content="<?php echo get_field('townscape_short_description');?>" />

  <!-- Schema.org markup for Google+ -->
  <meta itemprop="name" content="<?php the_title_attribute();?>">
  <meta itemprop="description" content="<?php echo get_field('townscape_short_description');?>">
  <meta itemprop="image" content="<?php echo $image_url[0];?>">

  <!-- Twitter Card data -->
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@kenmaredotcom">
  <meta name="twitter:title" content="<?php the_title_attribute();?>">
  <meta name="twitter:description" content="<?php echo get_field('townscape_short_description');?>">
  <meta name="twitter:creator" content="@kenmaredotcom">
  <!-- Twitter summary card with large image must be at least 280x150px -->
  <meta name="twitter:image:src" content="<?php echo $image_url[0];?>">

  <!-- Open Graph data -->
  <meta property="og:title" content="<?php the_title_attribute();?>" />
  <meta property="og:type" content="place" />
  <meta property="og:url" content="<?php the_permalink();?>" />
  <meta property="og:image" content="<?php echo $image_url[0];?>" />
  <meta property="og:description" content="<?php echo get_field('townscape_short_description');?>" />
  <meta property="og:site_name" content="Kenmare.com" />
  <meta property="place:location:latitude" content="<?php echo get_field('townscape_gps_lat');?>" />
  <meta property="place:location:longitude" content="<?php echo get_field('townscape_gps_lng');?>" />

<?php } ?>

  <link rel="alternate" type="application/rss+xml" title="<?php echo get_bloginfo('name'); ?> Feed" href="<?php echo esc_url(get_feed_link()); ?>">

  <?php wp_head(); ?>

</head>
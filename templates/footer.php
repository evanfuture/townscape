<section class="feature section">
	<?php
//	guidebook_locker();
	?>
</section>

<footer class="site-footer" role="contentinfo">
	<h4><?php echo get_bloginfo('name');;?></h4>
	<div class="sitemap section">
		<?php wp_nav_menu( array( 'theme_location' => 'sitemap_navigation' ) ); ?>
	</div>
	<div class="widgets section">
		<?php // dynamic_sidebar('sidebar-footer'); ?>
	</div>
	<div class="social-footer section">
		<a href="https://www.facebook.com/kenmaredotcom"><span class="icon-facebook social"></span></a>
		<a href="https://twitter.com/kenmaredotcom"><span class="icon-twitter social"></span></a>
		<a href="https://plus.google.com/+Kenmaredotcom"><span class="icon-googleplus social"></span></a>
		<a href="mailto:info@kenmare.com"><span class="icon-email social"></span></a>
	</div>
	<div class="credits section">
		<p><?php echo get_bloginfo('name' );?> is designed and maintained by <a href="http://kenmarecreative.com">Kenmare Creative</a>.  All Rights Reserved.</p>
    </div>
</footer>

<?php wp_footer(); ?>

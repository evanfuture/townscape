<?php
    if(is_attachment()){
        $attachment_id = get_the_ID(); // attachment ID
        $attachment_meta = wp_get_attachment($attachment_id);
        $image_metadata = wp_get_attachment_metadata($attachment_id );
        $image_title = $attachment_meta['title'];
        $image_alt = $attachment_meta['alt'];
        $image_src = $attachment_meta['src'];
        $page_subtitle = $attachment_meta['caption'];
        $page_description = $attachment_meta['description'];
        $image_meta = $image_metadata['image_meta'];
        $image_camera = $image_meta['camera'];
        $icon_class = 'icon-images';
    }
    elseif (is_tax('business_type')) {
        $queried_object = get_queried_object();
        $taxonomy = $queried_object->taxonomy;
        $term_id = $queried_object->term_id;
        $term_parent_id = $queried_object->parent;
            $icon_class = get_field('townscape_icon', $taxonomy . '_' . $term_id);
            $page_subtitle = get_field('townscape_subtitle', $taxonomy . '_' . $term_id);
            $page_description = term_description( $term_id, $taxonomy );

            $termy = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
            $parent = get_term($termy->parent, get_query_var('taxonomy') );

        if ($term_parent_id !=0){
            $proper_term_id = $term_parent_id;
            $parent_slug = $parent->slug;
            $parent_name = $parent->name;
        } else {
            $proper_term_id = $term_id;
            $parent_slug = $termy->slug;
            $parent_name = $termy->name;
        }
        $args = array('orderby'=> 'count','order'=> 'DESC','hide_empty'=> false, 'child_of'=> $proper_term_id);
        $terms = get_terms($taxonomy, $args);



    } else {
            $icon_class = get_field('townscape_icon');
            $page_subtitle = get_field('townscape_subtitle');
            $page_description = get_field('townscape_short_description');
    }
?>



<?php
    if(is_archive()){} elseif( has_post_thumbnail() || is_attachment() ) {?>
    <header class="page-image">
        <?php if(is_attachment()){ ?>
            <img class="img-responsive" alt="<?php echo $image_alt;?>" src="<?php echo $image_src;?>">
            <?php get_the_post_thumbnail( $post_id = $attachment_id, $size = 'townscape_full', $attr = array('class' => 'img-responsive') );?>
        <?php } else {
    	    the_post_thumbnail('townscape_full', array('class' => 'img-responsive'));
        }?>
    </header>
	<?php }?>

<div class="section page-header">

        <div class="page-title">
            <h2><?php echo roots_title(); ?></h2>
        </div>

        <?php if (!empty($page_subtitle)) { ?>
            <div class="page-subtitle">
                <h3><span class="<?php echo $icon_class;?>"></span><?php echo $page_subtitle;?></h3>
            </div>
        <?php } ?>
        <?php if (!empty($image_camera)) { ?>
            <div class="page-subtitle">
                <h3>Camera: <?php echo $image_camera;?></h3>
            </div>
        <?php } ?>
        <?php if (!empty($page_description)) { ?>
            <div class="page-description">
                <?php echo $page_description ?>
            </div>
        <?php } ?>
</div><!--/page-header-->

    <?php
    if ( !empty( $terms ) && !is_wp_error( $terms ) ) { ?>
    <?php
        $count = count($terms);
        $i=0;
        $term_list = '<h4>Browse <a href="/'. $parent_slug . '">' . $parent_name . '</a>: </h4><ul class="sidebar-content">';
        foreach ($terms as $term) {
            $i++;
            $term_icon = get_field('townscape_icon', $term->taxonomy . '_' . $term->term_id);
            $term_list .= '<li><a href="' . get_term_link( $term ) . '" title="' . sprintf(__('View all listings in %s', 'my_localization_domain'), $term->name) . '"><span class="' . $term_icon . '"></span>' . $term->name . ' (' . $term->count . ')</a></li>';
        }
        $term_list .= '</ul>';
        ?>
        <div class="sorting section">
            <?php echo $term_list; ?>
        </div>
    <?php } ?>
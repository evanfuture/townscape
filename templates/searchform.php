<div class="page-sidebar">
	<h4>Search: </h4>
	<div class="sidebar-content">
		<form role="search" method="get" class="search-form form-inline" action="<?php echo esc_url(home_url('/')); ?>">
		  <label class="sr-only"><?php _e('Search for:', 'roots'); ?></label>
		  <div class="input-group">
		    <input type="search" value="<?php echo get_search_query(); ?>" name="s" class="search-field form-control" placeholder="<?php _e('Search', 'roots'); ?> <?php bloginfo('name'); ?>">
		    <span class="input-group-btn">
		      <button type="submit" class="search-submit btn btn-default"><?php _e('Find', 'roots'); ?></button>
		    </span>
		  </div>
		</form>
		<h5>You can also browse our entire site through the menu below.</h5>
	</div>
</div>
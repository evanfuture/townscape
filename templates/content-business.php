<script>
var trackOutboundLink = function(url) {
         ga('send', 'event', 'outbound', 'click', url, {'hitCallback':
           function () {
           document.location = url;
           }
         });
      };
</script>

<?php
    $post_id = get_the_id();
    $user_id = get_current_user_id();
    $bookmarks = (array) get_user_meta($user_id, '_wpb_bookmarks', true);
    if (isset($bookmarks[$post_id])){
        $bookmarked_class = 'bookmarked';
    }
?>
<div itemscope itemtype="http://schema.org/LocalBusiness" class="page-content <?php echo $bookmarked_class?>">
    <div class="right-half">
        <?php if ( has_post_thumbnail() ) {
            if (is_single()) {
                the_post_thumbnail('townscape_listing', array('class' => 'listing-main-image'));
            } else {
                the_post_thumbnail('townscape_thumb', array('class' => 'listing-main-image'));
            }
        }
        else{
            $title = get_the_title();
            $stringtitle = str_replace(" ", "+", $title);
            echo '<img src="http://placehold.it/300x195&text='.$stringtitle.'" class="listing-main-image">';
        } ?>
       <?php //if gallery { <div class="image-gallery" ></div> };?>
       <?php if (is_single()){?>
                <?php
                $location_lat = get_field('townscape_gps_lat');
                $location_lng = get_field('townscape_gps_lng');
                $site_lat = get_field('townscape_site_gps_lat', 'option');
                $site_lng = get_field('townscape_site_gps_lng', 'option');
                if( !empty($location_lat) ):?>
                  <div class="mobile-map">
                    <img src="http://api.tiles.mapbox.com/v4/kenmarecreative.hjp8kh05/pin-s+d00(<?php echo $location_lng; ?>,<?php echo $location_lat; ?>),pin-s+888(<?php echo $site_lng; ?>,<?php echo $site_lat; ?>)/auto/350x240.png?access_token=pk.eyJ1Ijoia2VubWFyZWNyZWF0aXZlIiwiYSI6IkhJUlB4MTAifQ.72y0YoPhg0ZS33tVStGxZQ" />
                  </div>
                  <div class="geo" itemprop="geo" itemscope itemtype="http://schema.org/GeoCoordinates">
                        <a href="https://www.google.com/maps/dir/Current+Location/<?php echo $location_lat;?>,<?php echo $location_lng;?>" alt="Directions via Google Maps">Get Directions to <?php the_title();?></a><br/>

                        Latitude: <?php echo $location_lat;?><br />
                        Longitude: <?php echo $location_lng;?>
                    <meta itemprop="latitude" content="<?php echo $location_lat;?>" />
                    <meta itemprop="longitude" content="<?php echo $location_lng;?>" />
                  </div>
                  <div class="distance-from-town">
                  <?php
                  $distance = townscape_distance($location_lat, $location_lng, $site_lat, $site_lng, "M");
                  $rounded_distance = ceil($distance);
                  if($rounded_distance <=1 ){echo 'In Town.';} else {
                  echo $rounded_distance . " Miles from Town.<br>";
                  }
                  ?>
                  </div>
                <?php endif; ?>
        <?php }?>
    </div>
    <div class="left-half">
        <div class="card-name">
            <h2 itemprop="name" class="listing-name title">
                <?php the_title();?>
            </h2>
            <?php
                $subtitle = get_field('townscape_subtitle');
                if( !empty($subtitle)) {?>
                      <h3 class="subtitle"><?php echo $subtitle;?></h3>
                <?php }
            ?>
            <?php if (is_single()){?>
                <?php
                /**
                    $wifi = get_field('townscape_wifi');
                    $accessibility = get_field('townscape_wheelchair');
                    if ($wifi == "yes"){ ?>
                        <div class="wifi" alt="wifi"><span class='icon-wifi'></span></div>
                    <?php }
                    if ($accessibility == "yes"){?>
                        <div class="accessibility" alt="accessibility"><span class='icon-accessibility'></span></div>
                    <?php }
                    **/
                ?>
                <?php
                    $description = get_field('townscape_short_description');
                    if( !empty($description)) {?>
                         <div class="description" itemprop="description"><?php echo $description;?></div>
                    <?php }
                ?>
            <?php } ?>
        </div>

        <?php $card_type = get_post_type();?>

        <?php if ($card_type == 'business'):?>
            <?php
                $url = get_field('townscape_url');
                // Find connected pages
                $surl = new WP_Query( array(
                  'connected_type' => 'business_to_surl',
                  'connected_items' => get_queried_object(),
                ) );
                $title2 = get_the_title();

                // Display connected pages
                if ( $surl->have_posts() ) : while ( $surl->have_posts() ) : $surl->the_post(); ?>
                    <?php $url2 = get_permalink();?>
                    <a alt="Visit <?php echo $title2;?>" href="<?php echo $url2;?>" onclick="trackOutboundLink('<?php echo $url2;?>'); return false;" class="website-link primary-button" itemprop="url" target="_blank">Visit Website</a>
                    <?php if( !empty($url)) {?>
                        <a style="display:none" href="<?php echo $url;?>" class="sameAs" itemprop="sameAs"><?php echo $url;?></a>
                    <?php }?>
                <?php endwhile; wp_reset_postdata();
                else:
                    if( !empty($url)) {?>
                     <a alt="Visit <?php the_title();?>" href="<?php echo $url;?>" onclick="trackOutboundLink('<?php echo $url;?>'); return false;" class="website-link primary-button" itemprop="url" target="_blank">Visit Website</a>
                <?php }
                endif;
            ?>
            <?php
                $booking_com_url = get_field('townscape_booking_com_url');
                if( !empty($booking_com_url)) {?>
                    <a alt="Book <?php the_title();?> at Booking.com" href="<?php echo townscape_booking_com_affiliate_add($booking_com_url);?>" class="website-link primary-button booking-button">Book Now</a>
                <?php }
            ?>
            <div class="card-links">
            <?php
                $phone = get_field('townscape_phone');
                $email = get_field('townscape_email');
                $address1 = get_field('townscape_address1');
                $address2 = get_field('townscape_address2');
                $country = get_field('townscape_site_country', 'option');
            ?>
            <?php if (!empty($phone)) { ?>
                <div class="phone" itemprop="telephone">
                    <a class="listing-phone" href="tel:<?php echo $phone;?>"><span class="icon-phone"></span><?php echo $phone;?></a>
                </div>
            <?php } ?>
            <?php if (!empty($email)) { ?>
                <div class="email" itemprop="email">
                    <a class="listing-email" href="mailto:<?php echo $email;?>"><span class="icon-email"></span><?php echo $email;?></a>
                </div>
            <?php } ?>
            <?php if (!empty($address1)) { ?>
                <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress" class="address">
                    <span class="icon-location"></span>
                    <p class="listing-address">
                        <?php the_title();?><br/><span itemprop="streetAddress"><?php echo $address1;?></span><br/><span itemprop="addressLocality"><?php echo $address2;?></span></br><span itemprop="addressCountry"><?php echo $country;?></span>
                    </p>
                </div>
            <?php } ?>

            </div>

            <?php /**
            <!-- <div class="owner (public (adopted | orphaned) | private)" >
                <?php //if (public):?>
                    <?php //if(adopted):?>
                        This Public Listing is sponsored by: <?php //echo $owner_link;?>
                    <?php //else if(orphaned):?>
                        <a href="#" alt="Sponsor this Listing">This Public Listing can be sponsored.</a>
                    <?php //endif;?>
                <?php //endif; ?>
                <?php //if ( is_admin() ) {?>
                    <div class="billing-details" >
                        <div class="username">
                            Owner: <?php //echo $username_link; ?>
                        </div>
                        <div class="yearly-rate">
                            Rate: <?php //echo $yearly_rate;?> per year
                        </div>
                        <div class="payment (paid | due | overdue)">
                            Payment due in <?php //echo $expiry_calc; ?> days.
                        </div>
                        <div class="invoice-status (invoiced | not-invoiced)">
                        Invoiced
                        </div>
                    </div>
                <?php //} ?>
            </div>  -->


          <!--
            <div class="business-type-fields">
                <div class="accommodation">

                    <div class="rates">
                    €40 per (night|week|month).
                    <a href="#">Link to Rates on business Website</a>

                    </div>
                    <div class="Free Wifi">

                    </div>
                    <div class="terms-list for ammenities">

                    </div>
                </div>
                <div class="dining">
                    <div class="Free Wifi" >
                    </div>
                    <div class="Opening Hours" >
                    </div>
                    <div class="Pricing" >
                        <div class="1, 2 or 3" >
                        </div>
                    </div>
                    <div class="Half Portions available?" >
                    </div>
                    <div class="Meals Served" >
                        <div class="Breakfast" >
                        </div>
                        <div class="Lunch" >
                        </div>
                        <div class="Dinner" >
                        </div>
                        <div class="Bar Food" >
                        </div>
                        <div class="Early Bird" >
                        </div>
                        <div class="Kids Menu" >
                        </div>
                    </div>
                    <div class="Cuisines Served" >
                        <div class="Irish" >
                        </div>
                        <div class="Chinese" >
                        </div>
                        <div class="Etc." >
                        </div>
                    </div>
                    <div class="" >
                    </div>
                </div>
                <div class="activity">
                    <div class="Opening Hours" >
                    </div>
                    <div class="Age" >
                        <div class="6+ or 12+ or 18+, etc." >
                        </div>
                    </div>
                    <div class="Pricing" >
                        <div class="paid" >
                            <div class="display price" >
                            </div>
                        </div>
                        <div class="free" >
                        </div>
                    </div>
                    <div class="Duration" >
                        <div class="short" >
                        </div>
                        <div class="medium" >
                        </div>
                        <div class="long" >
                        </div>
                    </div>
                    <div class="Environment" >
                        <div class="mountain" >
                        </div>
                        <div class="beach" >
                        </div>
                        <div class="forest" >
                        </div>
                        <div class="varied" >
                        </div>
                        <div class="indoors" >
                        </div>
                    </div>
                    <div class="Activity Type" >
                        <div class="family" >
                        </div>
                        <div class="outdoors" >
                        </div>
                        <div class="event" >
                            <div class="dates" >
                            </div>
                            <div class="sponsors" >
                            </div>
                        </div>
                        <div class="route/tour" >
                            <div class="walk, drive or cycle" >
                            </div>
                            <div class="waypoints" >
                                <div class="manually added (gps coordinates)" >
                                </div>
                                <div class="Other businesses (gps from activity)" >
                                </div>
                            </div>
                            <div class="map scope" >
                            </div>
                        </div>
                        <div class="shopping" >
                        </div>
                    </div>
                </div>
                <div class="local-services">
                    <div class="" >
                    </div>
                </div>
            </div>
            -->
            <!-- <div class="related-content"/></div> --><!--Roadmapped for 2.1-->
            **/
            ?>
        <?php endif;?>
    </div>
    <div class="long-description">
        <?php the_content();?>
        <a href="/collections" alt="View my Collection of Listings">View my Collection of Listings</a>
    </div>
</div>

<?php //get_template_part('templates/content', 'user-form');?>

<script>
  if(window.location.hash) {
    var hash = window.location.hash.substring(1);
    $('article').append('<div class="beara-link"><a href="http://ringofbeara.com/'+hash+'" alt="Return to our sister website">Back to RingofBeara.com</a></div>');
  }
</script>

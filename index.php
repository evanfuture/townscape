<?php
$card_class = get_post_type();
$card_classes = array(
	'card',
	$card_class
	);
?>

<div class="feature section">
  <?php while (have_posts()) : the_post(); ?>
    <article <?php post_class($card_classes);?>>

		<?php
			if (is_attachment()){
				get_template_part('templates/page', 'header');
			}
			elseif (is_singular('business')) {
				$id_of_post = get_the_ID();
			    if ( current_user_can('edit_post', $id_of_post) ) {
			        // get_template_part('templates/content', 'user-form');
			        get_template_part('templates/content', 'business');
			    } else {
			    	get_template_part('templates/content', 'business');
		    	}
			}
			elseif (is_page() || is_singular('post') || is_singular('gem')) {
				get_template_part('templates/page', 'header');
					?>
				<div class="page-content">
	                <?php the_content();?>
	            </div><!-- /page-content-->
			<?php }
			else {?>
			<div class="page-content">
	                <?php the_content();?>
	            </div><!-- /page-content-->
			<?php }
		?>

      </article>
  <?php endwhile; ?>
</div>
<?php
/*
Template Name: Custom Template
*/
?>
<script src='https://api.tiles.mapbox.com/mapbox.js/v2.0.0/mapbox.js'></script>
<link href='https://api.tiles.mapbox.com/mapbox.js/v2.0.0/mapbox.css' rel='stylesheet' />
<script src='https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/leaflet.markercluster.js'></script>
<link href='https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/MarkerCluster.css' rel='stylesheet' />
<link href='https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/MarkerCluster.Default.css' rel='stylesheet' />
<style>
  #map { width:100%; height:400px;}
</style>
<?php get_template_part('templates/page', 'header'); ?>
<div class="section feature">
<script src="https://www.mapbox.com/mapbox.js/assets/data/realworld.388.js"></script>
<div id='map'></div>
</div>
<script>
L.mapbox.accessToken = 'pk.eyJ1Ijoia2VubWFyZWNyZWF0aXZlIiwiYSI6IkhJUlB4MTAifQ.72y0YoPhg0ZS33tVStGxZQ';
    var map = L.mapbox.map('map', 'examples.map-i86nkdio')
        .setView([-37.82, 175.215], 14);

    var markers = new L.MarkerClusterGroup();

    for (var i = 0; i < addressPoints.length; i++) {
        var a = addressPoints[i];
        var title = a[2];
        var marker = L.marker(new L.LatLng(a[0], a[1]), {
            icon: L.mapbox.marker.icon({'marker-symbol': 'post', 'marker-color': '0044FF'}),
            title: title
        });
        marker.bindPopup(title);
        markers.addLayer(marker);
    }

    map.addLayer(markers);
</script>